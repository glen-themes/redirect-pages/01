// oh boy
// this is basically just me practicing my shitty js
// i wish i was smarter

$(document).ready(function(){

var godspeed = getComputedStyle(document.documentElement)
              .getPropertyValue("--Typewriter-Speed");
              
// ****** typewriter effect by Rick Hitchcock ****** //
// https://stackoverflow.com/a/29462670/8144506 //
function printSentence(){
    var index = 0,
        the_text = $(".text-area").html(),
        timer = setInterval(function(){
            var char = the_text.charAt(index);
            if(char === "<") index = the_text.indexOf(">",index);
            document.getElementById("feed").innerHTML = the_text.substr(0,index);
            if(++index === the_text.length){
                clearInterval(timer);
            }
      }, godspeed);
};


    var getval = $("[typewriter]").attr("typewriter");
    if(getval == "yes"){
        setTimeout(printSentence, 400);

        function th(){
            var textheight = $(".text-area").height();
            $("#feed").height(textheight);
        }

        th();

        setTimeout(() => {
            th()
        },1200)

        $(window).load(function(){
            th()
        })

        document.addEventListener("DOMContentLoaded", () => {
            th()
        })
        
    } else {
        $(".text-area").addClass("nt");
        $(".links-blob").delay(750).fadeIn(250);
    }

// ****** character (text) count by Roxoradev ****** //
// https://stackoverflow.com/a/38102673/8144506 //
$(window).load(function(){
    var cont = $(".text-area").html();
        cont = cont.replace(/<[^>]*>/g,"");
        cont = cont.replace(/\s+/g, ' ');
        cont = cont.trim();
    var n = cont.length,
        xyz = n * godspeed;
    
    if(getval == "yes"){
        $(".links-blob").delay(xyz).fadeIn(250);
        $(".text-area").delay(xyz).remove();
    }
    
    // ****** time format fix by me ****** //
    var $thing = $(".timeandshit"),
        time = $thing.text(),
        clean = time.split("-").pop(),
        cut = clean.substring(0, clean.indexOf("M") + "M".length);
    $thing.text(cut);
    
    $(".timeandshit").html(function(_, html) {
       return html.replace(/(AM)/g, '<span class="am-pm">AM</span>')
                  .replace(/(PM)/g, '<span class="am-pm">PM</span>');
    });
    
    var $weekday = $(".weekday"),
        weekday_t = $weekday.text(),
        short = weekday_t.substring(0, weekday_t.indexOf(',')).split("-").pop()
                      .replace("Sunday", "Sun.")
                      .replace("Monday", "Mon.")
                      .replace("Tuesday", "Tue.")
                      .replace("Wednesday", "Wed.")
                      .replace("Thursday", "Thu.")
                      .replace("Friday", "Fri.")
                      .replace("Saturday", "Sat.");
    $weekday.text(short);
    
    var $monthday = $(".monthday"),
        monthday_t = $monthday.text(),
        sm = monthday_t.split(",")[1],
        reorder = sm.split(' ')[2]+' '+sm.split(' ')[1];
    $monthday.text(reorder);
    
    $(".clock").show();
});

// ****** detect wallpaper load by Colin ****** //
// https://stackoverflow.com/a/11055141/8144506 //
var $ac = $(".background-image"),
    bg = $ac.css("background-image");
    
// if (bg) {
//     var src = bg.replace(/(^url\()|(\)$|[\"\'])/g, ''),
//         $img = $("<img>").attr("src", src).on("load", function(){
//                     $ac.fadeIn();
//                });
// }

// fallback
// src: https://stackoverflow.com/a/2342181/8144506
if(bg){
    var sth = bg.replace(/(^url\()|(\)$|[\"\'])/g, '');
    var img = new Image();
    img.onload = function() { $ac.fadeIn(); }
    img.src = sth;
}

});// end ready
